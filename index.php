<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ciudad Minecraft</title>
        <link rel='shortcut icon' type='image/x-icon' href='favicon.png'/>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        
       <link rel="stylesheet" href="css/styles.css">
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-       Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-     wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>
    <body>
        <header class="container img-responsive">
            <a href="index.php"><img src="logo.png" style="display: block;"></a>
            <a href="index.php" class="mr-4" style="display: inline-block;"><h6 style="color: black; text-decoration: none;">Inicio</h6></a>
            <a href="archivo-de-la-nacion.php" class="mr-4" style="display: inline-block;"><h6 style="color: black; text-decoration: none;">Archivo de la Nación</h6></a>
            <a href="documentacion.php" class="mr-4" style="display: inline-block;"><h6 style="color: black; text-decoration: none;">Documentación</h6></a>
            <a href="cuenta.php" class="mr-4" style="display: inline-block;"><h6 style="color: black; text-decoration: none;">Mi Cuenta CDMC</h6></a>
        </header>
        <div class="container img-responsive">
            <img src="banner.png" class="img-fluid">
        </div>
        <div class="container">
            <center><h2 class="mt-5"><b>Conoce Más:</b></h2></center>
            <h5 class="mt-1">La Ciudad Minecraft fue fundada en 2016 por PlayerlGaming, y, hasta ahora se ha mantenido como el mejor mundo de roleplay de
            ciudad en Minecraft, tenemos un sistema de economía <b>justo</b> y <b>real</b> llamado MineCoins, este consta de una moneda que tiene cambios,
            puedes invertir en la bolsa, emprender, o trabajar para una empresa ya existente.</h5>
            <center><h2 class="mt-5"><b>Archivo de la nación:</b></h2></center>
            <h5 class="mt-1 mb-5"><a href="archivo-de-la-nacion.php" style="text-decoration: none; color: #61ffa3;">Listado de operaciones y documentación.</a></h5>
            
        </div>
<footer class="container page-footer font-small pt-4 mt-5">
  <div class="container-fluid text-center text-md-left">
    <div class="row">
      <div class="col-md-6 mt-md-0 mt-3">
        <h5 class="text-uppercase">Ciudad Minecraft</h5>
        <p>Ciudad de Roleplay en Minecraft</p>
        <p class="mt-1" style="color: deepskyblue;">Ciudad de Inovación</p>
        </div>
      <hr class="clearfix w-100 d-md-none pb-3">
      <div class="col-md-3 mb-md-0 mb-3">
        <h5 class="text-uppercase">Enlaces</h5>
        <ul class="list-unstyled">
          <li>
            <a href="archivo-de-la-nacion.php" style="text-decoration: none; color: #61ffa3;">Archivo de la Nación</a>
          </li>
          <li>
            <a href="documentacion.php" style="text-decoration: none; color: #61ffa3;">Documentación</a>
          </li>
          <li>
            <a href="correos.php" style="text-decoration: none; color: #61ffa3;">Correos CDMC</a>
          </li>
          <li>
            <a href="bangob.php" style="text-decoration: none; color: #61ffa3;">Bangob</a>
          </li>
        </ul>
         </div>
      <div class="col-md-3 mb-md-0 mb-3">
        <ul class="list-unstyled">
          <li>
            <a href="cuenta.php" style="text-decoration: none; color: #61ffa3;">Cuenta CDMC</a>
          </li>
          <li>
            <a href="denuncias.php" style="text-decoration: none; color: #61ffa3;">Seguimiento de Denuncias</a>
          </li>
          <li>
            <a href="registro-civil.php" style="text-decoration: none; color: #61ffa3;">Registro Civil</a>
          </li>
          <li>
            <a href="mapa.php" style="text-decoration: none; color: #61ffa3;">Mapa</a>
          </li>
          <li>
            <a href="https://discord.gg/AknRHxX" style="text-decoration: none; color: #61ffa3;">Discord</a>
          </li>
        </ul>

      </div>
    </div>
  </div>
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
      <a href="https://cdmc.playerlgg.xyz"> cdmc.playerlgg.xyz</a><p>
      <a href="legal/aviso-de-privacidad.pdf" class="mr-2">Aviso de Privacidad</a>
      <a href="legal/tyc-y-uso-justo.pdf" class="ml-2">Términos y Condiciones y Uso Justo</a><p>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
    </body>
</html>